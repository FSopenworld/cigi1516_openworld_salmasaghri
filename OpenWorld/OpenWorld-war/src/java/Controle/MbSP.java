/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;




import com.GestionSpecialites.SpecialiteSessionBeanLocal;
import entites.Specialite;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.*;


@ManagedBean
@SessionScoped
public class MbSP {
    @EJB
    private SpecialiteSessionBeanLocal specialiteSessionBean;
  
    

 private Specialite specialite;
 private List<Specialite> specialites;
 private Specialite editSpecialite;

   

 
 @PostConstruct
 public void init(){
    
     specialite=new Specialite();
    
 }

    public Specialite getEditSpecialite() {
        return editSpecialite;
    }

    public void setEditSpecialite(Specialite editSpecialite) {
        this.editSpecialite = editSpecialite;
    }


    public Specialite getSpecialite() {
        return specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

    public List<Specialite> getSpecialites() {
        specialites=specialiteSessionBean.selectAllSpecialite();
        return specialites;
    }

    public void setSpecialites(List<Specialite> specialites) {
        this.specialites = specialites;
    }
 
 

  
    public String AddSpecialite(){
        
        specialiteSessionBean.addSpecialite(specialite);
        return "ListeSpecialites";
    }
    
 public String edit(Specialite ad) {
	    
		this.editSpecialite=ad;
                return "ModifSpecialite";
	}
    public String validEdit() {
  
        editSpecialite=specialiteSessionBean.update(editSpecialite);
        return "ListeSpecialites";
		
	}
    
     public String deleteSpecailite(Specialite sp) {
	    
		specialiteSessionBean.deleteSpecialite(sp);
                return "ListeSpecialites";
	}
    
}
