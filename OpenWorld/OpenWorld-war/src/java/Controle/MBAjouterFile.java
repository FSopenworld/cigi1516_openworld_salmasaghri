/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import GestionEtudiant.GestionetudiantLocal;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author pc
 */
@ManagedBean
@RequestScoped
public class MBAjouterFile {
    @EJB
    private GestionetudiantLocal gestionetudiant;

   private Publication doc;
   private UploadedFile file ;
   private List<Publication> listdoc;
   private List<Offre> listoffre;
    @Resource(mappedName = "jmsDS")
    private Topic statusMessageTopic;
  
    @Resource(mappedName = "jmsTopicTC")
    private TopicConnectionFactory staticMessageTopicCF;

     private String subject="Demande de stage ";
    private String body="OpenWorld: Réseau social profesionnel \n ";
    public List<Offre> getListoffre() {
        return listoffre;
    }

    public void setListoffre(List<Offre> listoffre) {
        this.listoffre = listoffre;
    }
   

    public GestionetudiantLocal getGestionetudiant() {
        return gestionetudiant;
    }

    public void setGestionetudiant(GestionetudiantLocal gestionetudiant) {
        this.gestionetudiant = gestionetudiant;
    }

    public List<Publication> getListdoc() {
        return listdoc;
    }

    public void setListdoc(List<Publication> listdoc) {
        this.listdoc = listdoc;
    }


    public Publication getDoc() {
        return doc;
    }

    public void setDoc(Publication doc) {
        this.doc = doc;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    @PostConstruct
    public void init(){
        doc = new Publication();
        listdoc = gestionetudiant.listdocuments();
        listoffre= gestionetudiant.listoffre();
    }

    public String goTo(){
         InputStream is;
        String fileName = file.getFileName();
        String dir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("images");
         String dir3 = FacesContext.getCurrentInstance().getExternalContext().encodeActionURL( "C:\\Users\\pc\\Desktop\\Nouveau dossier");
           System.out.println(dir3);
         //  FacesContext.getCurrentInstance().getExternalContext().getAbsolutePath();
         System.out.println(dir+"\\"+fileName);
        String dir1 = "C:\\Users\\pc\\Desktop\\Nouveau dossier";
            System.out.println(fileName);
        try {

             is = file.getInputstream();
             byte buf[] = new byte[is.available()];
             is.read(buf);
            File tempFile = new File(dir,fileName);

             try (OutputStream os = new FileOutputStream(tempFile)) {
                 os.write(buf);
             }
            is.close();
            Type p = gestionetudiant.TypePub();
            
          
            doc.setIdType(p);
            doc.setContenu(fileName);
            MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
            doc.setIdUtilisateur(f.getE());
            gestionetudiant.ajoutpublication(doc);
             listdoc = gestionetudiant.listdocuments();
            System.out.println(doc.toString());
        } catch (IOException ex) {
            System.out.println(ex);
        }

        return"Etudiant_espace";
    }
    public StreamedContent FileDownloadView(String item) throws MalformedURLException, IOException {
//        System.out.println("hhhhhhhhhhh");
//         File tempFile = new File("C:/Users/pc/Desktop/Nouveaudossier/conventionfadwa.docx");
//        StreamedContent  file1= new DefaultStreamedContent(new FileInputStream(tempFile), new MimetypesFileTypeMap().getContentType(tempFile));
//        URL url = new URL("file:///C:/Users/pc/Desktop/Nouveaudossier/conventionfadwa.docx");
//        URLConnection connection = url.openConnection();
//        InputStream in = connection.getInputStream();
       // InputStream stream = new InputStream("C:\\Users\\pc\\Desktop\\Nouveau dossier\\");
//      StreamedContent  file1 = new DefaultStreamedContent(in ,"docx");
        String path;
    String contentType;
        path =FacesContext.getCurrentInstance().getExternalContext().getRealPath("images")+"/"+item;
        contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
   return new DefaultStreamedContent(new FileInputStream(path), contentType,item);
   // return file1;
    }
         public String postuler(Offre o) throws JMSException, UnknownHostException{
         
              MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        if(o.getIdEnseignant()!=null && o.getIdEntreprise()==null ) 
        {
             o.getEtudiants().add(f.getE());
             f.getE().getOffres().add(o);
             gestionetudiant.updateE(f.getE());
         gestionetudiant.updateoffre(o);
        body=body+" \n Suite à votre publication sur la platforme OpenWorld je postule à votre offre intitulé par "+o.getOffre()+" \n Veuillez consulter mon profile voila mon CNE"+f.getE().getCNE();
        sendJMSMessageToJmsDR(o.getIdEnseignant().getEmail(),subject,body);
       return "affichoffre";
        }
         if(o.getIdEnseignant()==null && o.getIdEntreprise()!=null ) 
        {
             o.getEtudiants().add(f.getE());
             f.getE().getOffres().add(o);
             gestionetudiant.updateE(f.getE());
         gestionetudiant.updateoffre(o);
body=body+" \n Suite à votre publication sur la platforme OpenWorld je postule à votre offre intitulé par "+o.getOffre()+" \n Veuillez consulter mon profile voila mon CNE"+f.getE().getCNE();        sendJMSMessageToJmsDR(o.getIdEntreprise().getEmail(),subject,body);
       return "affichoffre";
        }
        
     return "affichoffre";
              }
 private void sendJMSMessageToJmsDR(String email, String subject, String body) throws JMSException, UnknownHostException {
        
       Connection connection = staticMessageTopicCF.createConnection();
        connection.start();
        Session topicSession = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        MessageProducer publisher = topicSession.createProducer(statusMessageTopic);
        MapMessage message = topicSession.createMapMessage();
        message.setStringProperty("body", body);
        message.setStringProperty("subject", subject);
        message.setStringProperty("email", email);
        publisher.send(message);
    }
}
