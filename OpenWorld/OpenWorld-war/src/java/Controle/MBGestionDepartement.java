/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import GestionDepartement.GestionDepartementLocal;
import entites.Departement;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author FADOUA
 */
@ManagedBean(name="departementlist")
@SessionScoped
public class MBGestionDepartement {
    @EJB
    private GestionDepartementLocal gestionDepartement;
    List<Departement> departementList;
    private Departement dep;


    /**
     * Creates a new instance of MBGestionDepartement
     */
    public MBGestionDepartement() {
    }

    
    public Departement getDep() {
        return dep;
    }

    public void setDep(Departement dep) {
        this.dep = dep;
    }


    
    public List<Departement> getDepartementList(){
        departementList = gestionDepartement.ListeDepartement();
        return departementList;
    }
    
    public String modification(Departement departement){
        this.dep = departement;
         System.out.println("modification"); 
        System.out.println(dep.getDepartement());
  
       return "DepModification";
        
    }
    
    public String update(){
  //inserer le departement avec modification
        
        dep = gestionDepartement.updateDepartement(dep);
        
          return "DepartementList";
    }
    
    public String suppression(Departement dep){
        
        gestionDepartement.DeleteDepartement(dep);
       
        return "DepartementList";
    }
}
