/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import GestionEspace.EspaceLaureatLocal;
import entites.Etudiant;
import entites.Laureat;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author FADOUA
 */
@ManagedBean
@RequestScoped
public class MBLaureat {
    @EJB
    private EspaceLaureatLocal espaceLaureat;

    /**
     * Creates a new instance of MBLaureat
     */
   
    
      private List<Offre> offrelist;
    private Offre offre;
    private Laureat laureat;
    private List<Offre> offrestage;
    private List<Offre> offremploi;
    private int selecetedTypeOffre;
     private Publication doc;
   private UploadedFile file ;
   private List<Publication> listdoc;
   private StreamedContent file2;
   
   //jms
       @Resource(mappedName = "jmsDS")
    private Topic statusMessageTopic;
  
    @Resource(mappedName = "jmsTopicTC")
    private TopicConnectionFactory staticMessageTopicCF;

     private String subject="Demande de stage ";
    private String body="OpenWorld: RÃ©seau social profesionnel \n ";
      //espace laureat
    @PostConstruct
    public void init(){
       
        offre = new Offre();
        doc = new Publication();
        listdoc = new ArrayList<Publication>();
      listdoc = espaceLaureat.doc();
       
    } 

    public EspaceLaureatLocal getEspaceLaureat() {
        return espaceLaureat;
    }

    public void setEspaceLaureat(EspaceLaureatLocal espaceLaureat) {
        this.espaceLaureat = espaceLaureat;
    }

    public List<Offre> getOffrelist() {
        return offrelist;
    }

    public void setOffrelist(List<Offre> offrelist) {
        this.offrelist = offrelist;
    }

    public List<Offre> getOffrestage() {
        return offrestage;
    }

    public void setOffrestage(List<Offre> offrestage) {
        this.offrestage = offrestage;
    }

    
    public List<Offre> getOffremploi() {
        return offremploi;
    }

    public void setOffremploi(List<Offre> offremploi) {
        this.offremploi = offremploi;
    }

    public Publication getDoc() {
        return doc;
    }

    public void setDoc(Publication doc) {
        this.doc = doc;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<Publication> getListdoc() {
      
        return listdoc;
    }

    public void setListdoc(List<Publication> listdoc) {
        this.listdoc = listdoc;
    }

    public StreamedContent getFile2() {
        return file2;
    }

    public void setFile2(StreamedContent file2) {
        this.file2 = file2;
    }

    
   
    public Offre getOffre() {
        return offre;
    }

    public void setOffre(Offre offre) {
        this.offre = offre;
    }

  

    public int getSelecetedTypeOffre() {
        return selecetedTypeOffre;
    }

    public void setSelecetedTypeOffre(int selecetedTypeOffre) {
        this.selecetedTypeOffre = selecetedTypeOffre;
    }

    public Laureat getLaureat() {
        return laureat;
    }

    public void setLaureat(Laureat laureat) {
        this.laureat = laureat;
    }

    
    //publier un offre
    public String publierOffre(Etudiant laureat){
         Date date=new Date();
         offre.setDatePublication(date.toString());
        System.out.println("selected"+selecetedTypeOffre);
        if(selecetedTypeOffre == 1){
            offre.setTypeOffre(0);
        offre.setIdLaureat(laureat);
        espaceLaureat.publierOffre(offre);
            System.out.println("stage");
        
        }else
        if(selecetedTypeOffre  == 2){
            offre.setTypeOffre(1);
            offre.setIdLaureat(laureat);
        espaceLaureat.publierOffre(offre);
            System.out.println("emploi");
        }
         if(selecetedTypeOffre == 1)
             return "OffreStage";
         else 
             return "OffrEmploi";
        
    }
    
      public void add(){
        
        InputStream is;
        String fileName = file.getFileName();
        String dir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("images");
         System.out.println(dir+"\\"+fileName);
        String dir1 = "C:\\Users\\pc\\Desktop\\Nouveau dossier";
            System.out.println(fileName);
        try {

             is = file.getInputstream();
             byte buf[] = new byte[is.available()];
             is.read(buf);
      File tempFile = new File(dir+"\\"+fileName);
            OutputStream os = new FileOutputStream(tempFile);

            os.write(buf);
            os.close();
            is.close();
            Type p = espaceLaureat.Type();
            
            Date d = new Date();
		
	System.out.println(d.toString());
        doc.setDatePub(d.toString());
            doc.setIdType(p);
            doc.setContenu(fileName);
            MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
            doc.setIdUtilisateur(f.getE());
            espaceLaureat.addDoc(doc);
        } catch (IOException ex) {
        }

      
      listdoc = espaceLaureat.doc();
       
//        return"Etudiant_espace";
    }
      
      //telecherger in document
       public StreamedContent FileDownloadView(String contenu) throws MalformedURLException, IOException {
//        System.out.println("hhhhhhhhhhh");
//         File tempFile = new File("C:/Users/pc/Desktop/Nouveaudossier/conventionfadwa.docx");
//        StreamedContent  file1= new DefaultStreamedContent(new FileInputStream(tempFile), new MimetypesFileTypeMap().getContentType(tempFile));
//        URL url = new URL("file:///C:/Users/pc/Desktop/Nouveaudossier/conventionfadwa.docx");
//        URLConnection connection = url.openConnection();
//        InputStream in = connection.getInputStream();
       // InputStream stream = new InputStream("C:\\Users\\pc\\Desktop\\Nouveau dossier\\");
//      StreamedContent  file1 = new DefaultStreamedContent(in ,"docx");
        String path;
    String contentType;
        path =FacesContext.getCurrentInstance().getExternalContext().getRealPath("images")+"/"+contenu;
        contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
   return new DefaultStreamedContent(new FileInputStream(path), contentType,contenu);
   // return file1;
    }
       
       //consulter une offre
        public String consulter(Offre offre) {
        this.offre=offre;
        return "visualiser";
    }
        
        public String postuler() throws JMSException, UnknownHostException{
              MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        if(offre.getIdEnseignant()!=null && offre.getIdEntreprise()==null &&  offre.getIdLaureat() ==null) 
        {
             offre.getEtudiants().add(f.getE());
             f.getE().getOffres().add(offre);
             espaceLaureat.updateE(f.getE());
        espaceLaureat.updateoffre(offre);
        body=body+" \n Suite Ã  votre publication sur la platforme OpenWorld je postule Ã  votre offre intitulÃ© par "+offre.getOffre()+" \n Veuillez consulter mon profile voila mon CNE"+f.getE().getCNE();
        sendJMSMessageToJmsDR(offre.getIdEnseignant().getEmail(),subject,body);
       return "Laureat_espace";
        }
         if(offre.getIdEnseignant()==null && offre.getIdEntreprise()!=null &&  offre.getIdLaureat() ==null) 
        {
             offre.getEtudiants().add(f.getE());
             f.getE().getOffres().add(offre);
             espaceLaureat.updateE(f.getE());
         espaceLaureat.updateoffre(offre);
body=body+" \n Suite Ã  votre publication sur la platforme OpenWorld je postule Ã  votre offre intitulÃ© par "+offre.getOffre()+" \n Veuillez consulter mon profile voila mon CNE"+f.getE().getCNE(); 
sendJMSMessageToJmsDR(offre.getIdEntreprise().getEmail(),subject,body);

       return "Laureat_espace";
        }else  if(offre.getIdEnseignant()==null && offre.getIdEntreprise()==null && offre.getIdLaureat() !=null) 
        {
             offre.getEtudiants().add(f.getE());
             f.getE().getOffres().add(offre);
             espaceLaureat.updateE(f.getE());
         espaceLaureat.updateoffre(offre);
body=body+" \n Suite Ã  votre publication sur la platforme OpenWorld je postule Ã  votre offre intitulÃ© par "+offre.getOffre()+" \n Veuillez consulter mon profile voila mon CNE"+f.getE().getCNE(); 
sendJMSMessageToJmsDR(offre.getIdLaureat().getEmail(),subject,body);

       return "Laureat_espace";
        }
        
     return "Laureat_espace";
              }
    
         private void sendJMSMessageToJmsDR(String email, String subject, String body) throws JMSException, UnknownHostException {
        
       Connection connection = staticMessageTopicCF.createConnection();
        connection.start();
        Session topicSession = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        MessageProducer publisher = topicSession.createProducer(statusMessageTopic);
        MapMessage message = topicSession.createMapMessage();
        message.setStringProperty("body", body);
        message.setStringProperty("subject", subject);
        message.setStringProperty("email", email);
        publisher.send(message);
    }
}
