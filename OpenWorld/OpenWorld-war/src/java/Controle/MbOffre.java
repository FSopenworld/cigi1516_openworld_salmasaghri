
package Controle;


import com.GestionOffre.OffreSessionBeanLocal;
import entites.Enseignant;
import entites.Entreprise;
import entites.Etudiant;
import entites.Offre;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@RequestScoped
public class MbOffre{
   
    @EJB
    private OffreSessionBeanLocal offreSessionBean;
 
    private Offre offre;
    private List<Offre> offres;
    private Set<Etudiant> candidats;
    private Entreprise profil;
    private Etudiant etudiantByCNE;
    private String CNE;
    


 @PostConstruct
 public void init(){
    
     offre=new Offre();
     offres=new ArrayList<Offre>();
     candidats=new HashSet<Etudiant>();
     profil=new Entreprise();
     
     etudiantByCNE=new Etudiant();
     
 }

    public Offre getOffre() {
        return offre;
    }

    public List<Offre> getOffres() {
        return offres;
    }
    
      public List<Offre> ListOffres(Entreprise idEntreprise) {
         
        offres=offreSessionBean.selectAllOffreEnt(idEntreprise);
        return offres;
    }

    public void setOffres(List<Offre> offres) {
        this.offres = offres;
    }

    public Set<Etudiant> getCandidats() {
        return candidats;
    }

    public void setCandidats(Set<Etudiant> candidats) {
        this.candidats = candidats;
    }

    public Entreprise getProfil() {
        MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        profil=f.getEn();
        return profil;
    }

    public void setProfil(Entreprise profil) {
        this.profil = profil;
    }

    
 public String addOffre(Entreprise idEntreprise){
     Date date=new Date();
     offre.setIdEntreprise(idEntreprise);
     offre.setDatePublication(date.toString());
  
    if(offreSessionBean.addOffre(offre))
        
        return "Entreprise_espace";
      return "AjoutOffreEnt";
 }
 public String addOffreEnseignant(Enseignant idEnseignant){
     Date date=new Date();
     offre.setIdEnseignant(idEnseignant);
     offre.setDatePublication(date.toString());
  
    if(offreSessionBean.addOffre(offre))
        
        return "Enseignant_espace";
      return "AjoutOffreEnsg";
 }
   
    public Long getNbPostulant(Long id){
        return offreSessionBean.nBPostulant(id);
    }

    public Etudiant getEtudiantByCNE() {
        return etudiantByCNE;
    }

    public void setEtudiantByCNE(Etudiant etudiantByCNE) {
        this.etudiantByCNE = etudiantByCNE;
    }

    public String getCNE() {
        return CNE;
    }

    public void setCNE(String CNE) {
        this.CNE = CNE;
    }
    
  public String goToCandidat(Offre off){
      candidats=off.getEtudiants();
      return "DemandeCandidat";
  }
   
  
  public String goToEtudiantProfil()
  {
      etudiantByCNE=offreSessionBean.getEtudiantByCne(CNE);
      return "AffichageProfilEtudiant";
  
  
  }
   public String rechercheEtudiant(){
        etudiantByCNE=offreSessionBean.getEtudiantByCne(CNE);
      
        return "EtudiantRecherche";
    }
  
  public String deleteOff(Offre of)
  {
      offreSessionBean.deleteOffre(of);
      
      return "Entreprise_espace";
  
  }
  public String logOut(){
  return "Home";
  }
}
