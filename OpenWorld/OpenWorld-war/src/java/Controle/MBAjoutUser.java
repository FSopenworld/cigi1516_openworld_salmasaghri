/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import com.GestionUtilisateur.GestionUtilisateurLocal;
import entites.*;
import entites.Utilisateur;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author ibn brahim
 */
@ManagedBean
@ViewScoped
public class MBAjoutUser {
    @EJB
    private GestionUtilisateurLocal gestionUtilisateur;
    private int selecetedTypeUser;
    private String etat= "display: none" ;
    private Utilisateur utilisateur=new Utilisateur();
    private Enseignant enseignant=new Enseignant();
    private Admin admin=new Admin();
    private Etudiant etudiant=new Etudiant();
    private Entreprise entreprise =new Entreprise();
    private List<Departement> Departements;
    private List<SelectItem> listDepartement;
    private long selectedDepartement;
    private List<Parcours> listParcours;
    private Set<SelectItem> listformation;
    private Set<SelectItem> listniveau = new HashSet<>();;
    private Set<SelectItem> listspecialite;
    private Set<Formation> listformation1;
    private Set<Niveau> listniveau1;
    private Set<Specialite> listspecialite1;
    private Long selectedformation ;
    private Long selectedniveau ;
    private Long selectedspecialite ;

    public List<Parcours> getList() {
        return listParcours;
    }

    public void setList(List<Parcours> list) {
        this.listParcours = list;
    }

    public Set<SelectItem> getListformation() {
        return listformation;
    }

    public void setListformation(Set<SelectItem> listformation) {
        this.listformation = listformation;
    }

    public Set<SelectItem> getListniveau() {
        return listniveau;
    }

    public void setListniveau(Set<SelectItem> listniveau) {
        this.listniveau = listniveau;
    }

    public Set<SelectItem> getListspecialite() {
        return listspecialite;
    }

    public void setListspecialite(Set<SelectItem> listspecialite) {
        this.listspecialite = listspecialite;
    }

    public Set<Formation> getListformation1() {
        return listformation1;
    }

    public void setListformation1(Set<Formation> listformation1) {
        this.listformation1 = listformation1;
    }

    public Set<Niveau> getListniveau1() {
        return listniveau1;
    }

    public void setListniveau1(Set<Niveau> listniveau1) {
        this.listniveau1 = listniveau1;
    }

    public Set<Specialite> getListspecialite1() {
        return listspecialite1;
    }

    public void setListspecialite1(Set<Specialite> listspecialite1) {
        this.listspecialite1 = listspecialite1;
    }

    public Long getSelectedformation() {
        return selectedformation;
    }

    public void setSelectedformation(Long selectedformation) {
        this.selectedformation = selectedformation;
    }

    public Long getSelectedniveau() {
        return selectedniveau;
    }

    public void setSelectedniveau(Long selectedniveau) {
        this.selectedniveau = selectedniveau;
    }

    public Long getSelectedspecialite() {
        return selectedspecialite;
    }

    public void setSelectedspecialite(Long selectedspecialite) {
        this.selectedspecialite = selectedspecialite;
    }
    
    

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
    
    
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public List<Departement> getDepartements() {
        return Departements;
    }

    public void setDepartements(List<Departement> Departements) {
        this.Departements = Departements;
    }

    public List<SelectItem> getListDepartement() {
        return listDepartement;
    }

    public void setListDepartement(List<SelectItem> listDepartement) {
        this.listDepartement = listDepartement;
    }

    public long getSelectedDepartement() {
        return selectedDepartement;
    }

    public void setSelectedDepartement(long selectedDepartement) {
        this.selectedDepartement = selectedDepartement;
    }
    
    

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }


    public int getSelecetedTypeUser() {
        return selecetedTypeUser;
    }

    public void setSelecetedTypeUser(int selecetedTypeUser) {
        this.selecetedTypeUser = selecetedTypeUser;
    }
    
    public void handleChange(){ 
     
        for(Parcours p : listParcours){
            if(p.getIdFormation().getId()==  selectedformation){
                listniveau1.add(p.getIdNiveau());
            }
        }
        for(Niveau n : listniveau1){
            listniveau.add(new SelectItem (n.getId(),n.getNiveau()));
        }
     
    }
    
    public void handleChange1(){ 
        for(Parcours p : listParcours){
            if(p.getIdNiveau().getId()==  selectedniveau && p.getIdFormation().getId()==  selectedformation ){
                listspecialite1.add(p.getIdSpecialite());
            }
        }
        for(Specialite s : listspecialite1){
            listspecialite.add(new SelectItem (s.getId(),s.getSpecialite()));
        }
    }
    @PostConstruct
    public void init(){
    MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
       if(!f.isIslogged()){
           ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() +"/faces/Home.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MBAjoutUser.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
        
    }
    
    
    public String goToPage(){
        etat = "display: block";
        
        if(selecetedTypeUser==2){
            listParcours= gestionUtilisateur.listParcours();
            listformation =new HashSet<>();
            listformation1= new HashSet<>();

            listspecialite =new HashSet<>();
            listniveau1 =new HashSet<>();
            listspecialite1 =new HashSet<>();
        
            for(Parcours p : listParcours){
            listformation1.add(p.getIdFormation());
                }
            for(Formation f : listformation1 ){
                listformation.add(new SelectItem (f.getId(),f.getFormation()));
            }
            
        }else if(selecetedTypeUser==3){
            Departements=new ArrayList<Departement>();
            listDepartement=new ArrayList<SelectItem>();
            Departements=gestionUtilisateur.ListeDepartements();
            for(Iterator iter =  Departements.iterator(); iter.hasNext();){
                Departement d=(Departement) iter.next();
                listDepartement.add(new SelectItem(d.getId(),d.getDepartement()));
            }
        }
        return "Ajout";
        
    }
    
    public String AjoutUser(){
        
        //AjoutAdmin
        if(selecetedTypeUser==1){
            admin.setAdresse(utilisateur.getAdresse());
            admin.setNom(utilisateur.getNom());
            admin.setDateNC(utilisateur.getDateNC());
            admin.setEmail(utilisateur.getEmail());
            admin.setTel(utilisateur.getTel());
            gestionUtilisateur.AjoutAdmin(admin);
         //Ajout Etudiant
        }else if(selecetedTypeUser==2){
            etudiant.setAdresse(utilisateur.getAdresse());
            etudiant.setNom(utilisateur.getNom());
            etudiant.setDateNC(utilisateur.getDateNC());
            etudiant.setEmail(utilisateur.getEmail());
            etudiant.setTel(utilisateur.getTel());
            for(Parcours p : listParcours ){
            if(p.getIdFormation().getId()== selectedformation && p.getIdNiveau().getId()== selectedniveau && p.getIdSpecialite().getId()== selectedspecialite){
                etudiant.setIdParcours(p);
                break;
            }
        }
            gestionUtilisateur.AjoutEtudiant(etudiant);
         
        //Ajout Enseignant
        }else if(selecetedTypeUser==3){
            enseignant.setAdresse(utilisateur.getAdresse());
            enseignant.setNom(utilisateur.getNom());
            enseignant.setDateNC(utilisateur.getDateNC());
            enseignant.setEmail(utilisateur.getEmail());
            enseignant.setTel(utilisateur.getTel());
            for(Iterator iter =  Departements.iterator(); iter.hasNext();){
                Departement d=(Departement) iter.next();
                if(d.getId()==selectedDepartement)
                    enseignant.setIdDepartement(d);
                    break;
            }
            gestionUtilisateur.AjouterEnseignant(enseignant);
        //AjoutEntreprise
        }else if(selecetedTypeUser==4){
            entreprise.setAdresse(utilisateur.getAdresse());
            entreprise.setNom(utilisateur.getNom());
            entreprise.setDateNC(utilisateur.getDateNC());
            entreprise.setEmail(utilisateur.getEmail());
            entreprise.setTel(utilisateur.getTel());
            entreprise.setVerifie(true);
            gestionUtilisateur.AjoutEntreprise(entreprise);
        }
        
        return "ListeUsers";
    }
}
