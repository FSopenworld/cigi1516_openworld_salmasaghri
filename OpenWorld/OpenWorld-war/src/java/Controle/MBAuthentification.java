/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import com.GestionUtilisateur.AuthentificationLocal;
import entites.Admin;
import entites.Diplome;
import entites.Enseignant;
import entites.Entreprise;
import entites.Etudiant;
import entites.Experience;
import entites.Offre;
import entites.Publication;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author pc
 */
@ManagedBean
@SessionScoped
public class MBAuthentification {
    @EJB
    private AuthentificationLocal authentification;
    private Etudiant e;
    private Admin a;
    private Entreprise en;
    private Enseignant ens;
    
    private Set<Diplome> ListeDiplomeEnseignant;
    private Set<Experience> ListeExperiencesEnseignant;
    private Set<Publication> ListePublicationEnseignant;
    private Set<Offre> ListeOffreEnseignant;
    
    private List<SelectItem> listEtudiant;
    private int selecteditem;
    private String login;
    private String mdp;
    private boolean islogged;
    private Etudiant laureat;

    public Etudiant getLaureat() {
        return laureat;
    }

    public void setLaureat(Etudiant laureat) {
        this.laureat = laureat;
    }

    public boolean isIslogged() {
        return islogged;
    }

    public void setIslogged(boolean islogged) {
        this.islogged = islogged;
    }
    

    public Set<Offre> getListeOffreEnseignant() {
         ListeOffreEnseignant =new HashSet<>();
       ListeOffreEnseignant=ens.getOffres();
        return ListeOffreEnseignant;
    }

    public void setListeOffreEnseignant(Set<Offre> ListeOffreEnseignant) {
        this.ListeOffreEnseignant = ListeOffreEnseignant;
    }
    
    public Set<Publication> getListePublicationEnseignant() {
         ListePublicationEnseignant =new HashSet<>();
       ListePublicationEnseignant=ens.getPublcations();
        return ListePublicationEnseignant;
    }

    public void setListePublicationEnseignant(Set<Publication> ListePublicationEnseignant) {
        this.ListePublicationEnseignant = ListePublicationEnseignant;
    }

    
    public Set<Diplome> getListeDiplomeEnseignant() {
        ListeDiplomeEnseignant =new HashSet<>();
       ListeDiplomeEnseignant=ens.getDiplomes();
        return ListeDiplomeEnseignant;
    }

    public void setListeDiplomeEnseignant(Set<Diplome> ListeDiplomeEnseignant) {
        this.ListeDiplomeEnseignant = ListeDiplomeEnseignant;
    }

    public Set<Experience> getListeExperiencesEnseignant() {
        ListeExperiencesEnseignant =new HashSet<>();
       ListeExperiencesEnseignant=ens.getExperiences();
        
        return ListeExperiencesEnseignant;
    }

    public void setListeExperiencesEnseignant(Set<Experience> ListeExperiencesEnseignant) {
        this.ListeExperiencesEnseignant = ListeExperiencesEnseignant;
    }
    
    

    public Admin getA() {
        return a;
    }

    public void setA(Admin a) {
        this.a = a;
    }

    public Entreprise getEn() {
        return en;
    }

    public void setEn(Entreprise en) {
        this.en = en;
    }

    public Enseignant getEns() {
        return ens;
    }

    public void setEns(Enseignant ens) {
        this.ens = ens;
    }

    
    
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public AuthentificationLocal getAuthentification() {
        return authentification;
    }

    public void setAuthentification(AuthentificationLocal authentification) {
        this.authentification = authentification;
    }

    public Etudiant getE() {
        return e;
    }

    public void setE(Etudiant e) {
        this.e = e;
    }

    

   

   

    
    @PostConstruct
    public void init(){
        e= new Etudiant();
        a = new Admin();
        ens = new Enseignant();
        en = new Entreprise();
         laureat = new Etudiant();
        islogged=false;
       
    } 
    public String goTo(){
        
        if("Etudiant".equals(authentification.authentification_type(login, mdp))){
         e =  authentification.authentification_et(login, mdp);
         if(e != null){
              if("laureat".equals(authentification.authentification_lau(e))){
             laureat = e;
             return "Laureat_espace";
         }
              else{
             if(e.getActive()== 1){
                 islogged=true;
             return "Etudiant_espace";
             }
             else {
          RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Compte Désactiver."));
              return "Home";
             }
              }
         }
         else {
          RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Données eronées."));

             return "Home";
         }
        }else if("Enseignant".equals(authentification.authentification_type(login, mdp))){
            ens =  authentification.authentification_ense(login, mdp);
         if(ens != null){
           
                 islogged=true;
             return "Enseignant_espace";
         }
           
         else return "Home";
        }else if("Entreprise".equals(authentification.authentification_type(login, mdp))){
            en =  authentification.authentification_entr(login, mdp);
         if(en != null){
             if(en.isVerifie()){
                 
                 islogged=true;
             return "Entreprise_espace";
             
             }else{
               RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Compte Desactivé."));
                
             }
                 
         }
         else{ 
              RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Données eronées."));

             return "Home";
         }
        }else if("Admin".equals(authentification.authentification_type(login, mdp))){
            a =  authentification.authentification_admin(login, mdp);
         if(a != null){
             
                 islogged=true;
             return "Accueil";
         }
         else return "Home";
        }
        return "Home";
    }
    public String logout(){
                 FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    return "Home";
    }
    
    
}
