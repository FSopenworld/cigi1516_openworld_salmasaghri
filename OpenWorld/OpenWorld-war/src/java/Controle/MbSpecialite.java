/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;




import com.GestionSpecialites.SpecialiteSessionBeanLocal;
import entites.Specialite;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


@ManagedBean
@RequestScoped
public class MbSpecialite {
   
    @EJB
    private SpecialiteSessionBeanLocal specialiteSessionBean;
  
    

 private Specialite specialite;

 @PostConstruct
 public void init(){
    
     specialite=new Specialite();
    
 }

    public Specialite getSpecialite() {
        return specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }


    public String specialiteAdd(){
            specialiteSessionBean.addSpecialite(specialite);
            return "ListeSpecialites";
    }
}
