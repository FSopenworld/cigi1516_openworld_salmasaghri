/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import GestionDepartement.GestionDepartementLocal;
import entites.Departement;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author FADOUA
 */
@ManagedBean(name="departement")
@RequestScoped
public class MBAjoutDepartement  {
    @EJB
    private GestionDepartementLocal gestionDepartement;

    /**
     * Creates a new instance of MBAjoutDepartement
     */
    public MBAjoutDepartement() {
    }
    
    Departement dep = new Departement();
    public Departement getDep() {
        return dep;
    }

    public void setDep(Departement dep) {
        this.dep = dep;
    }

    
    public String ajoutDepartement(){
        
        
        gestionDepartement.AjouterDepartement(dep);
        return "DepartementList";
    }
    
}
