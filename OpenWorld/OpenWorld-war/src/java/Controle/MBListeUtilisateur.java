/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import com.GestionUtilisateur.GestionUtilisateurLocal;
import entites.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author ibn brahim
 */
@ManagedBean(name = "mBListeUtilisateur")
@SessionScoped
public class MBListeUtilisateur {
    @EJB
    private GestionUtilisateurLocal gestionUtilisateur;
    
    private int selecetedTypeUser=0;
    String etatList= "display: block" ;
    String etatForm= "display: none" ;
    int etat=0;
    private List<Utilisateur> Utilisateurs;
    private List<Enseignant> enseignants;
    private List<Etudiant> etudiants;
    private List<Admin> admins;
    private List<Entreprise> entreprises;
    private Enseignant enseignant;
    private Etudiant etudiant;
    private Admin admin;
    private Entreprise entreprise;
    private Utilisateur utilisateur;
    private long selctedDepartement;
    private List<Departement> Departements;
    private List<SelectItem> listDepartement;
    private Long selectedformation ;
    private Long selectedniveau ;
    private Long selectedspecialite ;
    private List<Parcours> listParcours;
    private Set<SelectItem> listformation;
    private Set<SelectItem> listniveau = new HashSet<>();;
    private Set<SelectItem> listspecialite;
    private Set<Formation> listformation1;
    private Set<Niveau> listniveau1;
    private Set<Specialite> listspecialite1;

    public Long getSelectedformation() {
        return selectedformation;
    }

    public void setSelectedformation(Long selectedformation) {
        this.selectedformation = selectedformation;
    }

    public Long getSelectedniveau() {
        return selectedniveau;
    }

    public void setSelectedniveau(Long selectedniveau) {
        this.selectedniveau = selectedniveau;
    }

    public Long getSelectedspecialite() {
        return selectedspecialite;
    }

    public void setSelectedspecialite(Long selectedspecialite) {
        this.selectedspecialite = selectedspecialite;
    }

    public List<Parcours> getListParcours() {
        return listParcours;
    }

    public void setListParcours(List<Parcours> listParcours) {
        this.listParcours = listParcours;
    }

    public Set<SelectItem> getListformation() {
        return listformation;
    }

    public void setListformation(Set<SelectItem> listformation) {
        this.listformation = listformation;
    }

    public Set<SelectItem> getListniveau() {
        return listniveau;
    }

    public void setListniveau(Set<SelectItem> listniveau) {
        this.listniveau = listniveau;
    }

    public Set<SelectItem> getListspecialite() {
        return listspecialite;
    }

    public void setListspecialite(Set<SelectItem> listspecialite) {
        this.listspecialite = listspecialite;
    }

    public Set<Formation> getListformation1() {
        return listformation1;
    }

    public void setListformation1(Set<Formation> listformation1) {
        this.listformation1 = listformation1;
    }

    public Set<Niveau> getListniveau1() {
        return listniveau1;
    }

    public void setListniveau1(Set<Niveau> listniveau1) {
        this.listniveau1 = listniveau1;
    }

    public Set<Specialite> getListspecialite1() {
        return listspecialite1;
    }

    public void setListspecialite1(Set<Specialite> listspecialite1) {
        this.listspecialite1 = listspecialite1;
    }
    
    

    public List<Departement> getDepartements() {
        return Departements;
    }

    public void setDepartements(List<Departement> Departements) {
        this.Departements = Departements;
    }

    public List<SelectItem> getListDepartement() {
        return listDepartement;
    }

    public void setListDepartement(List<SelectItem> listDepartement) {
        this.listDepartement = listDepartement;
    }
    
    

    public long getSelctedDepartement() {
        return selctedDepartement;
    }

    public void setSelctedDepartement(long selctedDepartement) {
        this.selctedDepartement = selctedDepartement;
    }
    
    

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
    
    
            
       

    public List<Entreprise> getEntreprises() {
        return entreprises;
    }

    public void setEntreprises(List<Entreprise> entreprises) {
        this.entreprises = entreprises;
    }
    
    
    public List<Etudiant> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

    public List<Admin> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Admin> admins) {
        this.admins = admins;
    }
    
    
    public List<Utilisateur> getUtilisateurs() {
        Utilisateurs=new ArrayList<Utilisateur>();
        Utilisateurs=gestionUtilisateur.ListeUtilisateur();
        return Utilisateurs;
    }

    public void setUtilisateurs(List<Utilisateur> Utilisateurs) {
        this.Utilisateurs = Utilisateurs;
    }

    public List<Enseignant> getEnseignants() {
        return enseignants;
    }

    public void setEnseignants(List<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }
    
    

    public int getSelecetedTypeUser() {
        return selecetedTypeUser;
    }

    public void setSelecetedTypeUser(int selecetedTypeUser) {
        this.selecetedTypeUser = selecetedTypeUser;
    }

    public String getEtatList() {
        return etatList;
    }

    public void setEtatList(String etatList) {
        this.etatList = etatList;
    }

    public String getEtatForm() {
        return etatForm;
    }

    public void setEtatForm(String etatForm) {
        this.etatForm = etatForm;
    }

    
    
    
    @PostConstruct
    public void init()
    {
        etat=0;
        etatList= "display: block" ;
        etatForm= "display: none" ;
        selecetedTypeUser=0;
        Utilisateurs=new ArrayList<Utilisateur>();
        Utilisateurs=gestionUtilisateur.ListeUtilisateur();
        etudiant = new Etudiant();
        enseignant = new Enseignant();
    }   
    
    public String goToPage(){
      
     if(selecetedTypeUser==1){
         admins=new ArrayList<Admin>();
         admins=gestionUtilisateur.listAdmin();
     }else if(selecetedTypeUser==2){
         etudiants=new ArrayList<Etudiant>();
         etudiants=gestionUtilisateur.listEtudiant();
     }else if(selecetedTypeUser==3){
         enseignants=new ArrayList<Enseignant>();
         enseignants=gestionUtilisateur.ListeEnseignants();
     }else if(selecetedTypeUser==4){
         entreprises=new ArrayList<Entreprise>();
         entreprises=gestionUtilisateur.listEntreprise();
     }
        
      return "ListeUsers";
    }
    
    
    //Modification
       public void handleChange(){ 
       
        listniveau =new HashSet<>();
        listniveau1=new HashSet<>();
        
        for(Parcours p : listParcours){
            if(p.getIdFormation().getId()==  selectedformation){
                listniveau1.add(p.getIdNiveau());
            }
        }
        for(Niveau n : listniveau1){
            listniveau.add(new SelectItem (n.getId(),n.getNiveau()));
        }
     
    }
    
    public void handleChange1(){ 
        listspecialite =new HashSet<>();
        for(Parcours p : listParcours){
            if(p.getIdNiveau().getId()==  selectedniveau && p.getIdFormation().getId()==  selectedformation ){
                listspecialite1.add(p.getIdSpecialite());
            }
        }
        for(Specialite s : listspecialite1){
            listspecialite.add(new SelectItem (s.getId(),s.getSpecialite()));
        }
    }
    
    
    public String Modification(Object o){
    
        etat=1;
        etatList= "display: none" ;
        etatForm= "display: block" ;
         
     if(selecetedTypeUser==1){
         
         this.admin=(Admin)o;
     }else if(selecetedTypeUser==2){
         
         this.etudiant=(Etudiant)o;
         this.selectedformation=etudiant.getIdParcours().getIdFormation().getId();
         this.selectedniveau=etudiant.getIdParcours().getIdNiveau().getId();
         this.selectedspecialite=etudiant.getIdParcours().getIdSpecialite().getId();
         
           listParcours= gestionUtilisateur.listParcours();
            listformation =new HashSet<>();
            listformation1= new HashSet<>();

            listspecialite =new HashSet<>();
            listniveau1 =new HashSet<>();
            listniveau =new HashSet<>();
            listspecialite1 =new HashSet<>();
            for(Parcours p : listParcours){
            listformation1.add(p.getIdFormation());
                }
            for(Formation f : listformation1 ){
                listformation.add(new SelectItem (f.getId(),f.getFormation()));
            }
            
           for(Parcours p : listParcours){
            if(p.getIdFormation().getId()==  selectedformation){
                listniveau1.add(p.getIdNiveau());
            }
        }
        for(Niveau n : listniveau1){
            listniveau.add(new SelectItem (n.getId(),n.getNiveau()));
        }
        
         for(Parcours p : listParcours){
            if(p.getIdNiveau().getId()==  selectedniveau && p.getIdFormation().getId()==  selectedformation ){
                listspecialite1.add(p.getIdSpecialite());
            }
        }
        for(Specialite s : listspecialite1){
            listspecialite.add(new SelectItem (s.getId(),s.getSpecialite()));
        }
        
            
     }else if(selecetedTypeUser==3){
        
        this.enseignant=(Enseignant)o;
        selctedDepartement=enseignant.getId();
        Departements=new ArrayList<Departement>();
        listDepartement=new ArrayList<SelectItem>();
           Departements=gestionUtilisateur.ListeDepartements();
            for(Iterator iter =  Departements.iterator(); iter.hasNext();){
                Departement d=(Departement) iter.next();
                listDepartement.add(new SelectItem(d.getId(),d.getDepartement()));
            }
     }else if(selecetedTypeUser==4){
        
        this.entreprise=(Entreprise)o;
     }
        return "Modification";
        
    }
    
    //Modification Utilisateur
   


    public String update(){
        
        etat=0;
         System.out.println(etatList);
         System.out.println(etatForm);
       
     if(selecetedTypeUser==1){
         admin=gestionUtilisateur.updateAdmin(admin);
         
     }else if(selecetedTypeUser==2){
         etudiant=gestionUtilisateur.updateEtudiant(etudiant);
        
     }else if(selecetedTypeUser==3){
        
        enseignant= gestionUtilisateur.updateEnseignant(enseignant);
     }else if(selecetedTypeUser==4){
        
        entreprise=gestionUtilisateur.updateEntreprise(entreprise);
     }
        
        return "ListeUsers";
    }
    
     public MBListeUtilisateur() {
    }

    
    //Suppression 
    public String Supprimer(Object o){
       if(selecetedTypeUser==1){
           admin=(Admin) o;
         gestionUtilisateur.DeleteAdmin(admin);
         admins=gestionUtilisateur.listAdmin();
         
     }else if(selecetedTypeUser==2){
         etudiant=(Etudiant)o;
         gestionUtilisateur.DeleteEtudiant(etudiant);
         etudiants=gestionUtilisateur.listEtudiant();
        
     }else if(selecetedTypeUser==3){
       enseignant=(Enseignant) o;
        gestionUtilisateur.DeleteEnseignant(enseignant);
        enseignants=gestionUtilisateur.ListeEnseignants();
     }else if(selecetedTypeUser==4){
        entreprise=(Entreprise) o;
        gestionUtilisateur.DeleteEntreprise(entreprise);
        entreprises=gestionUtilisateur.listEntreprise();
     }
       
        return "ListeUsers";
    }
    
    //method pour rederiger l'etudiant lors la demande de l'activation de son compte
    public String activCompte(){
        

       //on cherche l'etudiant qui vient de demander l'activation de compte

       //on cherche l'etudiant qui vient de demander l'actovation de compte

        Object obj = gestionUtilisateur.activeEtudiant(etudiant.getCNE());
        //si l'etudiant n'appartieni pas à letablissement
        //un msg de deonnes erones s'affiche
         if("etudiant inexistant".equals(obj))
            {
             RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Données eronées."));
            return "Etudiant";
        }else
         //s'il exist
         {

          Etudiant e=new Etudiant();   //rechercher l'etudiant
        e= (Etudiant)gestionUtilisateur.activeEtudiant(etudiant.getCNE());
        e.setLogin(etudiant.getLogin());
        e.setPassword(etudiant.getPassword());
        e.setActive(1);
        //lui ajouteé le login et le mot de passe
        e = gestionUtilisateur.updateEtudiant(e);
        RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Succes", "Vous Avez Activez Votre Compte avec succes,Veuillez d'authentifier pour se connecter à votre compte"));
        return "Home" ;


         }
        
    }
    
     //method pour rederiger l'enseignant lors la demande de l'activation de son compte
    public String demandeCompte(){
        
        Enseignant ens = new Enseignant();
       //on cherche l'enseignantt qui vient de demander l'activation de compte
        Object obj = gestionUtilisateur.activeEnseignant(enseignant.getCodeEns());
        //si l'etudiant n'appartieni pas à letablissement
        //un msg de deonnes erones s'affiche
         if("enseignant inexistant".equals(obj))
            {
             RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Données eronées."));
            return "Enseignant";
        }else
         //s'il exist
         {
             //rechercher l'enseignant
        ens= (Enseignant)obj;
        ens.setLogin(enseignant.getLogin());
        ens.setPassword(enseignant.getPassword());
        ens.setActive(1);
        //lui ajouteé le login et le mot de passe
        ens = gestionUtilisateur.updateEnseignant(ens);
  
        return "Home" ;
         }
    }
  
}
