/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import EspaceEnseignant.SLEspaceEnseignantLocal;
import GestionPublication.GestionPublicationLocal;
import com.GestionOffre.OffreSessionBeanLocal;
import entites.Departement;
import entites.Etudiant;
import entites.Publication;
import entites.Type;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author ibn brahim
 */
@ManagedBean
@ViewScoped
public class MBOpEnseignant {
    @EJB
    private GestionPublicationLocal gestionPublication;
    @EJB
    private SLEspaceEnseignantLocal sLEspaceEnseignant;
     @EJB
    private OffreSessionBeanLocal offreSessionBean;
     
      private Etudiant etudiantByCNE;

    public Etudiant getEtudiantByCNE() {
        return etudiantByCNE;
    }

    public void setEtudiantByCNE(Etudiant etudiantByCNE) {
        this.etudiantByCNE = etudiantByCNE;
    }
      
      

    
    //CNE de l etudaint recherche 
    private String CNE;
    
    private String etatList="display : block";
    private String etatform="display : none";
    
    private Publication publication;
    private Publication publicationModif;
    private List<Type> Types;
    private List<SelectItem> listType;
    private UploadedFile file ;
    private long selectedType;
    private List<Publication> listPublicationEnseignant;

    public List<Publication> getListPublicationEnseignant() {
         MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        listPublicationEnseignant=gestionPublication.selectAllPublicationEnseignant(f.getEns());
        return listPublicationEnseignant;
    }

    public void setListPublicationEnseignant(List<Publication> listPublicationEnseignant) {
        this.listPublicationEnseignant = listPublicationEnseignant;
    }
    
    

    public long getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(long selectedType) {
        this.selectedType = selectedType;
    }
    

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    

    public List<Type> getTypes() {
        return Types;
    }

    public void setTypes(List<Type> Types) {
        this.Types = Types;
    }

    public List<SelectItem> getListType() {
        return listType;
    }

    public void setListType(List<SelectItem> listType) {
        this.listType = listType;
    }
    
    

    public Publication getPublicationModif() {
        return publicationModif;
    }

    public void setPublicationModif(Publication publicationModif) {
        this.publicationModif = publicationModif;
    }
    
    

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public String getEtatList() {
        return etatList;
    }

    public void setEtatList(String etatList) {
        this.etatList = etatList;
    }

    public String getEtatform() {
        return etatform;
    }

    public void setEtatform(String etatform) {
        this.etatform = etatform;
    }
    

    public String getCNE() {
        return CNE;
    }

    public void setCNE(String CNE) {
        this.CNE = CNE;
    }
    
   
    
    public MBOpEnseignant() {
    }
    
    @PostConstruct
    public void init(){
        publication=new Publication();
        publicationModif=new Publication();
        listPublicationEnseignant=new ArrayList<Publication>();
         Types=new ArrayList<Type>();
         etudiantByCNE=new Etudiant();
            listType=new ArrayList<SelectItem>();
            Types=sLEspaceEnseignant.ListeType();
            for(Iterator iter =  Types.iterator(); iter.hasNext();){
                Type d=(Type) iter.next();
                listType.add(new SelectItem(d.getId(),d.getType()));
            }
    }
    
    public void ModificationPub(Publication p){
        publicationModif=p;
        etatList="display : none";
        etatform="display : block";
    }
    
    public String updatePublication(){
        etatList="display : block";
        etatform="display : none";
        return "Enseignant_espace";
    }
    
    public String deletePublication(){
        
        etatList="display : block";
        etatform="display : none";
        return "Enseignant_espace";
    }
     public String AjoutPublication(){
         if(selectedType==1){
        InputStream is;
        String fileName = file.getFileName();
        String dir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("images");
         System.out.println(dir+"\\"+fileName);
        String dir1 = "C:\\Users\\ibn  brahim\\Desktop\\Nouveau dossier";
            System.out.println(fileName);
        try {

             is = file.getInputstream();
             byte buf[] = new byte[is.available()];
             is.read(buf);
            File tempFile = new File(dir1+"\\"+fileName);

            OutputStream os = new FileOutputStream(tempFile);

            os.write(buf);
            os.close();
            is.close();
            
            publication.setContenu(fileName);
            } catch (IOException ex) {
        }
        }
         Date date=new Date();
         publication.setDatePub(date.toString());
            MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
            publication.setIdUtilisateur(f.getEns());
             for(Iterator iter =  Types.iterator(); iter.hasNext();){
                Type d=(Type)iter.next();
                System.out.println(selectedType);
                System.out.println(d.getId());
                if(d.getId()==selectedType)
                    publication.setIdType(d);
                    break;
            }
            
        
         if(gestionPublication.AjouterPublication(publication)){
             Types=sLEspaceEnseignant.ListeType();
             return "Enseignant_espace";
         }else 
             return "AjoutPublication";
         
     }
     
     public String DeletePublication(Publication p){
         gestionPublication.DeletePublication(p);
         return "Enseignant_espace";
     }
}
