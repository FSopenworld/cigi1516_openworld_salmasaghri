/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import com.gestionNotification.EntrepriseNotificationLocal;
import com.GestionUtilisateur.GestionUtilisateurLocal;
import entites.Entreprise;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Salma
 */
@ManagedBean
@RequestScoped
public class MBEntreprise {
    @Resource(mappedName = "jmsDS")
    private Topic statusMessageTopic;
  
    @Resource(mappedName = "jmsTopicTC")
    private TopicConnectionFactory staticMessageTopicCF;
   
    
    @EJB
    private GestionUtilisateurLocal gestionUtilisateur;
    
    @EJB
    private EntrepriseNotificationLocal entrepriseNotification;
    
   
    
    private List<Entreprise> entreprises;

    private Entreprise entreprise;
    private String passwordConfirm;
    private long nbEntrepriseNonVerifie;
    
    private String subject="validation de compte";
    private String body="OpenWorld: Réseau social profesionnel \n votre compte a été crée avec succes";
    
    
    
    
     @PostConstruct
    void init(){
        entreprise =new Entreprise();
    }

    public List<Entreprise> getEntreprises() {
        entreprises=entrepriseNotification.selectAllEntrNonV();
        return entreprises;
    }

    public void setEntreprises(List<Entreprise> entreprises) {
        this.entreprises = entreprises;
    }

    
    public long getNbEntrepriseNonVerifie() {
        
        nbEntrepriseNonVerifie=entrepriseNotification.getNbEntNonVerifie();
        return nbEntrepriseNonVerifie;
    }

    public void setNbEntrepriseNonVerifie(int nbEntrepriseNonVerifie) {
        this.nbEntrepriseNonVerifie = nbEntrepriseNonVerifie;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
   
   

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
    
    
     public String addEntreprise(){
        
       entreprise.setVerifie(false);
        if(passwordConfirm.equals(entreprise.getPassword()))
        { gestionUtilisateur.AjoutEntreprise(entreprise);
        return "Home";
        }else{
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Erreur", "Données eronées."));
          return "EntrepriseCreat"; // on doit se mettre d'accord sur l'opération à effectuer en cas d'échec
        }
    }
     
     public String verifieEntreprise(Entreprise et) throws JMSException, UnknownHostException{
         et.setVerifie(true);
         et=gestionUtilisateur.updateEntreprise(et);
        if(et.getEmail()!=null && et.getLogin()!=null && et.getPassword()!=null) 
        {
        body=body+" \n Informations : \n code de réference :"+et.getCodeRef()+" \n Nom d'utilisateur :"+et.getLogin()+" \n mot de passe : "+et.getPassword();
        sendJMSMessageToJmsDR(et.getEmail(),subject,body);
        return "ListeEntrepriseNonVerif";
        }
        
        return null;
    
     }

     private void sendJMSMessageToJmsDR(String email, String subject, String body) throws JMSException, UnknownHostException {
        
       Connection connection = staticMessageTopicCF.createConnection();
        connection.start();
        Session topicSession = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        MessageProducer publisher = topicSession.createProducer(statusMessageTopic);
        MapMessage message = topicSession.createMapMessage();
        message.setStringProperty("body", body);
        message.setStringProperty("subject", subject);
        message.setStringProperty("email", email);
        publisher.send(message);
    }
    
}
