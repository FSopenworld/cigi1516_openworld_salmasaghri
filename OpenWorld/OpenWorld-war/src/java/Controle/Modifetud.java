/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import GestionEtudiant.GestionetudiantLocal;
import static com.sun.faces.facelets.util.Path.context;
import entites.Activites;
import entites.Diplome;
import entites.Experience;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author pc
 */
@ManagedBean
@ViewScoped
public class Modifetud {
     @EJB
    private GestionetudiantLocal gestionetudiant;
    private Experience e ;
    private Diplome d;
    private Activites a ;
   Set<Experience> experiences;

    public GestionetudiantLocal getGestionetudiant() {
        return gestionetudiant;
    }

    public void setGestionetudiant(GestionetudiantLocal gestionetudiant) {
        this.gestionetudiant = gestionetudiant;
    }

    public Set<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(Set<Experience> experiences) {
        this.experiences = experiences;
    }

    public Experience getE() {
        return e;
    }

    public void setE(Experience e) {
        this.e = e;
    }

    public Diplome getD() {
        return d;
    }

    public void setD(Diplome d) {
        this.d = d;
    }

    public Activites getA() {
        return a;
    }

    public void setA(Activites a) {
        this.a = a;
    }
    @PostConstruct
    public void init(){
       a = new Activites();
       d = new Diplome();
       e = new Experience();
    }
    public void ajouterE(){
     MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        e.setIdEtudiant(f.getE());
      //  gestionetudiant.ajouterExperience(e);
       f.getE().getExperiences().add(e);
       gestionetudiant.updateE(f.getE());
                  ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        try {
            context.redirect(context.getRequestContextPath() +"/faces/affichage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MBAjoutUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void ajouterD(){
     MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        d.setIdEtudiant(f.getE());
      //  gestionetudiant.ajouterExperience(e);
       f.getE().getDiplomes().add(d);
       gestionetudiant.updateE(f.getE());
                  ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        try {
            context.redirect(context.getRequestContextPath() +"/faces/affichage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MBAjoutUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      public void ajouterA(){
     MBAuthentification f = (MBAuthentification)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("mBAuthentification");
        a.setIdEtudiant(f.getE());
      //  gestionetudiant.ajouterExperience(e);
       f.getE().getActivites().add(a);
       gestionetudiant.updateE(f.getE());
                  ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        try {
            context.redirect(context.getRequestContextPath() +"/faces/affichage.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MBAjoutUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
