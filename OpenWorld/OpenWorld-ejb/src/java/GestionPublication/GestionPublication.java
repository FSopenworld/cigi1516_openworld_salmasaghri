/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionPublication;


import entites.Enseignant;
import entites.Publication;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ibn brahim
 */
@Stateless
public class GestionPublication implements GestionPublicationLocal {

  @PersistenceContext(unitName = "OpenWorld-ejbPU")
    private EntityManager em;
   
  public List<Publication> selectAllPublicationEnseignant(Enseignant idEnseignant) {
        
        return em.createNamedQuery("PublicationEnseignant.findAll", Publication.class).setParameter("param",idEnseignant).getResultList();
    }
    //modifier un Publication
    public Publication updatePublication(Publication publication) {
        return em.merge(publication);
          }

    @Override
    //ajouter un publication de l'itablissement
    public boolean AjouterPublication(Publication publication) {
        try { 
            em.persist(publication);
          
            return true;
        } catch ( Exception e ) {
            return false;
        }
        
    }

    @Override
    //supprimer une Publication
    public void DeletePublication(Publication publication) {
      publication = em.merge(publication);
	em.remove(publication);
        
    }

    @Override
    public List<Publication> ListePublication() {
        //selectionner tous les dpartement de l'itablissement
        Query query = em.createNamedQuery("Publication.findAll",Publication.class);
          return query.getResultList();
    }
}
