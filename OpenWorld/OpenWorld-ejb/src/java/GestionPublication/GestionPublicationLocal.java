/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionPublication;

import entites.Enseignant;
import entites.Publication;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ibn brahim
 */
@Local
public interface GestionPublicationLocal {
      public List<Publication> selectAllPublicationEnseignant(Enseignant idEnseignant);

    public boolean AjouterPublication(Publication publication);
     public Publication updatePublication(Publication publication);
      public void DeletePublication(Publication publication);
       public List<Publication> ListePublication();
    
}
