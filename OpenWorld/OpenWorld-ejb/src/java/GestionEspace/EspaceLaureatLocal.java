/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionEspace;

import entites.Etudiant;
import entites.Laureat;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author FADOUA
 */
@Local
public interface EspaceLaureatLocal {
    public List<Offre> offre();
    public List<Offre> offrEmploi();
    public List<Offre> offreStage();
    public void publierOffre(Offre offre);
    public Type Type();
    public void addDoc(Publication pub);
    public List<Publication> doc();
    public Etudiant updateE(Etudiant e);
      public Offre updateoffre(Offre o);
    
}
