/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionEspace;

import entites.Etudiant;
import entites.Laureat;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FADOUA
 */
@Stateless
public class EspaceLaureat implements EspaceLaureatLocal {
    @PersistenceContext(unitName = "OpenWorld-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
   
    

    @Override
    //recuperer tous les offres
    public List<Offre> offre() {
        
        Query query = em.createNamedQuery("Offre.findAll", Offre.class);
        List l =query.getResultList();
        
    return l;
    }
    
    //recuperer les offre d'emploi
    public List<Offre> offrEmploi(){
        
        Query query = em.createNamedQuery("Offre.emploi", Offre.class);
        query.setParameter("param", 1);
        List l =query.getResultList();
        int i=0;
        for(i=0; i<l.size();i++){
            System.out.println(l.get(i));
        }
        return l;
    }
    
      
    //recuperer les offre de stage
    @Override
    public List<Offre> offreStage() {
     Query query = em.createNamedQuery("Offre.stage", Offre.class);
        query.setParameter("param", 0);
        List l =query.getResultList();
        int i=0;
        for(i=0; i<l.size();i++){
            System.out.println(l.get(i));
        }
        return l;
    }
    
    @Override
    public void publierOffre(Offre offre){
        
        em.persist(offre);
    }
    
    //recuperer le type du document
    public Type Type(){
        
        Query query = em.createNamedQuery("Type", Type.class);
        query.setParameter("param", "document");
        List list = query.getResultList();
        return (Type) list.get(0);
    }
    
    //ajouter une publication
    public void addDoc(Publication pub){
        em.persist(pub);
    }
    
    //liste des publication
    public List<Publication> doc(){
        
         Query query = em.createNamedQuery("pub", Publication.class);
        List list = query.getResultList();
        return list;
    }
    
       @Override
    public Offre updateoffre(Offre o){
         return em.merge(o);
    }

    @Override
    public Etudiant updateE(Etudiant e) {
        return em.merge(e);
    }
}
