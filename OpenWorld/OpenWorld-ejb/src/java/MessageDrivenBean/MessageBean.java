/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageDrivenBean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;


/**
 *
 * @author ibn brahim
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jmsDS"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jmsDS"),
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jmsDS"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class MessageBean implements MessageListener {
    
    @Resource(name = "mail/wineappMail")
    private Session mailwineappMail;
    
    public MessageBean() {
    }
    
    @Override
    public void onMessage(Message message) {
          try {
              
            MapMessage mMsg =(MapMessage)message;
            
            String body = mMsg.getStringProperty("body");
            String email = mMsg.getStringProperty("email");
            String subject = mMsg.getStringProperty("subject");
            
            
           try {
                sendMail(email,subject,body);
                
            } catch (NamingException ex) {
                Logger.getLogger(MessageBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(MessageBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("===> MDB : "+body);
            
        } catch (JMSException ex) {
            Logger.getLogger(MessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendMail(String email, String subject, String body) throws NamingException, MessagingException {
        MimeMessage message = new MimeMessage(mailwineappMail);
        message.setSubject(subject);
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setText(body);
        Transport.send(message);
    }
    
}
