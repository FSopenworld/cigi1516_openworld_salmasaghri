/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gestionNotification;

import entites.Entreprise;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Salma
 */
@Local
public interface EntrepriseNotificationLocal {
     public long getNbEntNonVerifie();
     public List<Entreprise> selectAllEntrNonV();
}
