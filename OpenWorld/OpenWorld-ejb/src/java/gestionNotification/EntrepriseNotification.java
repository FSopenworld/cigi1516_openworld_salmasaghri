/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionNotification;

import com.gestionNotification.EntrepriseNotificationLocal;
import entites.Entreprise;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Salma
 */
@Stateless
public class EntrepriseNotification implements EntrepriseNotificationLocal {
    
 @PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
 
  public long getNbEntNonVerifie() {
      return (long)em.createNamedQuery("getNbEntrNonVerif").getSingleResult();
   }
  
  public List<Entreprise> selectAllEntrNonV(){
  
        List<Entreprise> entreprises=new ArrayList<Entreprise>();
        Query query=em.createNamedQuery("Entreprise.findAllEntrNonVerif",Entreprise.class);
        entreprises=query.getResultList();
        
      return entreprises;
  }
  
  
}
