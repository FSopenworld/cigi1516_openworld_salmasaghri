/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionOffre;

import entites.Entreprise;
import entites.Etudiant;
import entites.Offre;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Salma
 */
@Local
public interface OffreSessionBeanLocal {
    public boolean addOffre(Offre s);
    public List<Offre> selectAllOffreEnt(Entreprise idEntreprise);
    public Long nBPostulant(Long idOffre);
    public Etudiant getEtudiantByCne(String CNE);
    public void deleteOffre(Offre off);
    
 
}
