/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionOffre;

import entites.Entreprise;
import entites.Etudiant;
import entites.Offre;
import java.util.Date;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Salma
 */
@Stateless
public class OffreSessionBean implements OffreSessionBeanLocal {
  @PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
  
  @Override
    public boolean addOffre(Offre s) {   
        
       try { 
            em.persist(s);
          
            return true;
        } catch ( Exception e ) {
            return false;
        }
    }

    @Override
    public List<Offre> selectAllOffreEnt(Entreprise idEntreprise) {
        
        return em.createNamedQuery("OffreEntreprise.findAll", Offre.class).setParameter("param",idEntreprise).getResultList();
    }

    @Override
    public Long nBPostulant(Long idOffre) {
         return (long)em.createNamedQuery("NbPostulantOffre").setParameter(1, idOffre).getSingleResult();
         }

    @Override
    public Etudiant getEtudiantByCne(String CNE) {
        
       return em.createNamedQuery("Etudiant.findByCNE", Etudiant.class).setParameter("cne", CNE).getSingleResult();
         }

    @Override
    public void deleteOffre(Offre u) {
     u = em.merge(u);
	em.remove(u);
    }

  


}