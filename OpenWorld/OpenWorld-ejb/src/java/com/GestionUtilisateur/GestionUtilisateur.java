/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionUtilisateur;

import entites.*;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ibn brahim
 */
@Stateless
public class GestionUtilisateur implements GestionUtilisateurLocal {

    @PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
    
    //Recuperation de la liste des Departements
    public List<Departement> ListeDepartements()
    {
        List<Departement> Departements=new ArrayList<Departement>();
        Query req=em.createNamedQuery("Departement.findAll",Departement.class);
        Departements=req.getResultList();
        return Departements;
    }
    
    //Ajout Enseignant
    public void AjouterEnseignant(Enseignant e){
        em.persist(e);
        
    }
    //Ajout Admin
    public void AjoutAdmin(Admin admin) {
      
            em.persist( admin );
      
    }
    //Ajout Entreprise
    public void AjoutEntreprise(Entreprise entreprise) {
      
            em.persist(entreprise);
    }
     
    //Ajout Etudiant
    public void AjoutEtudiant(Etudiant etudiant) {
      
            em.persist(etudiant);
    }
    
    //Laliste des Parcours
     public List<Parcours> listParcours() {
         Query query = em.createNamedQuery("Parcours.findAll",Parcours.class);
        List list = query.getResultList();
        return   list;
    }
  
    //La Liste des Enseignants
    public List<Enseignant> ListeEnseignants()
    {
        List<Enseignant> Enseignants=new ArrayList<Enseignant>();
        Query req=em.createNamedQuery("Enseignant.findAll",Enseignant.class);
        Enseignants=req.getResultList();
        return Enseignants;
    }
    
    //La Liste des Utilisateurs
    public List<Utilisateur> ListeUtilisateur()
    {
        List<Utilisateur> Utilisateurs=new ArrayList<Utilisateur>();
        Query req=em.createNamedQuery("Utilisateur.findAll",Utilisateur.class);
        Utilisateurs=req.getResultList();
        if(Utilisateurs.isEmpty())
            System.out.println("la liste est vide");
        return Utilisateurs;
    }
    
    //La liste des Etudiants 
     public List<Etudiant> listEtudiant() {
         Query query = em.createNamedQuery("Etudiant.findAll",Etudiant.class);
        List list = query.getResultList();
        return   list;
    }
     //La liste des Admin
     public List<Admin> listAdmin() {
         Query query = em.createNamedQuery("Admin.findAll",Admin.class);
        List list = query.getResultList();
        return   list;
    }
     //La liste des Entreprises 
     public List<Entreprise> listEntreprise() {
         Query query = em.createNamedQuery("Entreprise.findAll",Entreprise.class);
        List list = query.getResultList();
        return   list;
    }
      
   public Entreprise  updateEntreprise(Entreprise entreprise) {
       return em.merge(entreprise);
    }
   
   public Enseignant updateEnseignant(Enseignant enseignant) {
       return em.merge(enseignant);
    }
   public Admin updateAdmin(Admin admin) {
       return em.merge(admin);
    }
   public Etudiant updateEtudiant(Etudiant etudiant) {
       return em.merge(etudiant);
    }
    
   public void DeleteEtudiant(Etudiant etudiant) {
      etudiant=em.merge(etudiant);
      em.remove(etudiant);
    }
   
   public void DeleteEnseignant(Enseignant enseignant) {
        enseignant=em.merge(enseignant);
        em.remove(enseignant);
    }
   public void DeleteEntreprise(Entreprise entreprise) {
        entreprise=em.merge(entreprise);
        em.remove(entreprise);
    }
   public void DeleteAdmin(Admin admin) {
        admin=em.merge(admin);
        em.remove(admin);
    }

    @Override
    //demande d'activation du compte de l'etudiant
    public Object activeEtudiant(String cne) {
   
        Etudiant etudiant = new Etudiant();
        Query query = em.createNamedQuery("Etudiant.findByCNE", Etudiant.class);
        query.setParameter("cne", cne);
       List list = query.getResultList();
       if(list.size() == 0)
           return "etudiant inexistant";
       return (Object)list.get(0);
    }
    
    //demande d'activation du compte de l'enseignant
     public Object activeEnseignant(String codeEns){
          Enseignant enseignant = new Enseignant();
        Query query = em.createNamedQuery("Enseignant.codeEns", Enseignant.class);
        query.setParameter("param", codeEns);
       List list = query.getResultList();
       if(list.size() == 0)
           return "enseignant inexistant";
       return (Object)list.get(0);
     }
   
   
}
