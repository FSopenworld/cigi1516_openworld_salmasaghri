/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionUtilisateur;

import entites.Admin;
import entites.Enseignant;
import entites.Entreprise;
import entites.Etudiant;
import javax.ejb.Local;

/**
 *
 * @author pc
 */
@Local
public interface AuthentificationLocal {
    public Etudiant authentification_et(String login, String mdp);
    public Entreprise authentification_entr(String login, String mdp);
    public Admin authentification_admin(String login, String mdp);
     public Enseignant authentification_ense(String login, String mdp);
     public String authentification_type(String login,String mdp);
      public String authentification_lau(Etudiant e); 
    
}
