/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionUtilisateur;

import entites.Admin;
import entites.Enseignant;
import entites.Entreprise;
import entites.Etudiant;
import entites.Laureat;
import entites.Parcours;
import entites.Utilisateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pc
 */
@Stateless
public class Authentification implements AuthentificationLocal {
 @PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
    @Override
    public Etudiant authentification_et(String login, String mdp) {
          Query query = em.createNamedQuery("Etudiant.findByLogin",Etudiant.class);
          query.setParameter("login", login);
          query.setParameter("mdp", mdp);
        List list = query.getResultList();
        
        
        return (Etudiant) list.get(0);
        
    }

    @Override
    public Entreprise authentification_entr(String login, String mdp) {
 Query query = em.createNamedQuery("Entreprise.findByLogin",Etudiant.class);
          query.setParameter("login", login);
          query.setParameter("mdp", mdp);
        List list = query.getResultList();
        
        
        return (Entreprise) list.get(0);    }

    @Override
    public Admin authentification_admin(String login, String mdp) {
 Query query = em.createNamedQuery("Admin.findByLogin",Etudiant.class);
          query.setParameter("login", login);
          query.setParameter("mdp", mdp);
        List list = query.getResultList();
        
        
        return (Admin) list.get(0);    }

    @Override
    public Enseignant authentification_ense(String login, String mdp) {
 Query query = em.createNamedQuery("Enseignant.findByLogin",Etudiant.class);
          query.setParameter("login", login);
          query.setParameter("mdp", mdp);
        List list = query.getResultList();
        
        
        return (Enseignant) list.get(0);    }

    @Override
    public String authentification_type(String login, String mdp) {
         Query query = em.createNamedQuery("getTypeUser",Utilisateur.class);
         query.setParameter(1, login);
          query.setParameter(2, mdp);
          List<String>  s = query.getResultList();
          return s.get(0);
    }
 @Override
 public String authentification_lau(Etudiant e) {
        int n=0;
        String str="etudiant";
        Long l;
    Query query = em.createNamedQuery("Laureat.find", Laureat.class);
    List list = query.getResultList();
     for ( n = 0; n<list.size(); n++) {
         l = (Long) list.get(n);
         if(l == e.getId()){
             str ="laureat";
         break;
         }
     }
         return str;
       
    }
  
}
