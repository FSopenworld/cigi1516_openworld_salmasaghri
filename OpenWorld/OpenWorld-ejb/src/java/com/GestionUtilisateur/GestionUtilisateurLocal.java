/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionUtilisateur;

import entites.Admin;
import entites.Departement;
import entites.Enseignant;
import entites.Entreprise;
import entites.Etudiant;
import entites.Parcours;
import entites.Utilisateur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ibn brahim
 */
@Local
public interface GestionUtilisateurLocal {
    public List<Departement> ListeDepartements();
    public void AjouterEnseignant(Enseignant e);
    public List<Enseignant> ListeEnseignants();
    public List<Utilisateur> ListeUtilisateur();
    public List<Etudiant> listEtudiant();
    public List<Admin> listAdmin();
    public List<Entreprise> listEntreprise();
    public void AjoutAdmin(Admin admin);
    public void AjoutEntreprise(Entreprise entreprise);
    public void AjoutEtudiant(Etudiant entreprise);
    public Entreprise updateEntreprise(Entreprise entrepise);
    public Etudiant updateEtudiant(Etudiant etudiant);
    public Admin updateAdmin(Admin admin);
   public Enseignant updateEnseignant(Enseignant enseignant);
   public void DeleteAdmin(Admin admin);
    public void DeleteEnseignant(Enseignant enseignant);
    public void DeleteEtudiant(Etudiant etudiant);
    public void DeleteEntreprise(Entreprise entreprise);
    public Object activeEtudiant(String cne);     
   public Object activeEnseignant(String codeEns);     
    public List<Parcours> listParcours();
    
}
