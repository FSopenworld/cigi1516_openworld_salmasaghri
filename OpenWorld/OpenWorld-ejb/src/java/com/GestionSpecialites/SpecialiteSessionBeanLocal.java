/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionSpecialites;

import entites.Specialite;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Salma
 */
@Local
public interface SpecialiteSessionBeanLocal {
    
    List<Specialite> selectAllSpecialite();
    boolean addSpecialite(Specialite s);
    void deleteSpecialite(Specialite u);
    Specialite update(Specialite sp);
}
