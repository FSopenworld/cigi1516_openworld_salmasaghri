/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestionSpecialites;

import entites.Specialite;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Salma
 */
@Stateless
public class SpecialiteSessionBean implements SpecialiteSessionBeanLocal {
    
  @PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
  
    @Override
    public List<Specialite> selectAllSpecialite() {
        Query query = em.createNamedQuery("Specialite.findAll", Specialite.class);
        return   query.getResultList(); 
    }

    @Override
    public boolean addSpecialite(Specialite s) {   
        
       try { 
            em.persist(s);
          
            return true;
        } catch ( Exception e ) {
            return false;
        }
    }

    @Override
    public void deleteSpecialite(Specialite u) {
       u = em.merge(u);
	em.remove(u);
    }

    @Override
    public Specialite update(Specialite sp) {
	 return(em.merge(sp));
         
	
}
}
