/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionDepartement;

import entites.Departement;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author FADOUA
 */
@Local
public interface GestionDepartementLocal {

    /**
     *
     * @return
     */
    public List<Departement> ListeDepartement();
    public void AjouterDepartement(Departement departement);
    public Departement updateDepartement(Departement departement);
     public void DeleteDepartement(Departement departement);
     
    
}
