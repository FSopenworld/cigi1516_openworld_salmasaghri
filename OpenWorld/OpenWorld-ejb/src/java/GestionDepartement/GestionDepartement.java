/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionDepartement;

import entites.Departement;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FADOUA
 */
@Stateless
public class GestionDepartement implements GestionDepartementLocal {
    @PersistenceContext(unitName = "OpenWorld-ejbPU")
    private EntityManager em;
   

    //modifier un departement
    public Departement updateDepartement(Departement departement) {
        return em.merge(departement);
          }

    @Override
    //ajouter un departement de l'itablissement
    public void AjouterDepartement(Departement departement) {
        em.persist(departement);
        
    }

    @Override
    //supprimer un departement
    public void DeleteDepartement(Departement departement) {
      departement = em.merge(departement);
	em.remove(departement);
        
    }

    @Override
    public List<Departement> ListeDepartement() {
        //selectionner tous les dpartement de l'itablissement
        Query query = em.createNamedQuery("Departement.findAll",Departement.class);
          return query.getResultList();
    }
    


    
}
