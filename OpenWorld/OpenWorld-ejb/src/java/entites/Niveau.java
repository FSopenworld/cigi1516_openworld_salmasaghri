/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author FADOUA
 */
@Entity
@XmlRootElement
public class Niveau implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    private String niveau;
    
    //la liste des parcours d'un niveau
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idNiveau")
    private Set<Parcours> LesParcoursN= new HashSet();

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public Set<Parcours> getLesParcoursN() {
        return LesParcoursN;
    }

    public void setLesParcoursN(Set<Parcours> LesParcoursN) {
        this.LesParcoursN = LesParcoursN;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveau)) {
            return false;
        }
        Niveau other = (Niveau) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entites.Niveau[ id=" + id + " ]";
    }
    
}
