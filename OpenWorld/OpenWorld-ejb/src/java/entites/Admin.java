/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author FADOUA
 */
@Entity
@NamedQueries({
@NamedQuery(name="Admin.findAll", query="SELECT a FROM Admin a") ,
@NamedQuery(name = "Admin.findByLogin", query = "SELECT u FROM Admin u WHERE u.login = :login AND u.password= :mdp")})
public class Admin extends Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    private String prenom;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    

   
}
