/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author FADOUA
 */
@Entity
@NamedQueries({
@NamedQuery(name="Enseignant.findAll", query="SELECT e FROM Enseignant e")  ,
    @NamedQuery(name="Enseignant.codeEns", query="select e from Enseignant e where e.codeEns = :param"),
@NamedQuery(name = "Enseignant.findByLogin", query = "SELECT u FROM Enseignant u WHERE u.login = :login AND u.password= :mdp")})
public class Enseignant extends Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Column(unique=true)
    private String codeEns;
    private String prenom;
     private int active;
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEnseignant")
    private Set<Experience> experiences= new HashSet();
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEnseignant")
    private Set<Diplome> Diplomes= new HashSet();
    
    @ManyToOne(optional = false)
    private Departement idDepartement;
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEnseignant")
    private Set<Offre> Offres= new HashSet();

    public Set<Offre> getOffres() {
        return Offres;
    }

    public void setOffres(Set<Offre> Offres) {
        this.Offres = Offres;
    }

    
    public String getPrenom() {
        return prenom;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @XmlTransient
    public Set<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(Set<Experience> experiences) {
        this.experiences = experiences;
    }

    public Set<Diplome> getDiplomes() {
        return Diplomes;
    }

    public void setDiplomes(Set<Diplome> Diplomes) {
        this.Diplomes = Diplomes;
    }

    public Departement getIdDepartement() {
        return idDepartement;
    }

    public void setIdDepartement(Departement idDepartement) {
        this.idDepartement = idDepartement;
    }
    

    public String getCodeEns() {
        return codeEns;
    }

    public void setCodeEns(String codeEns) {
        this.codeEns = codeEns;
    }
    
    
    
}
