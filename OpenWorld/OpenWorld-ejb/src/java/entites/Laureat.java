/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author FADOUA
 */
@Entity 
@XmlRootElement
 @NamedNativeQuery(
                name    =   "Laureat.find",query   =   "select * from laureat"
        )
public class Laureat extends Etudiant implements Serializable {
    private static final long serialVersionUID = 1L;
    
    //la liste des parcours d'une Formation 
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idLaureat")
    private Set<Offre> Offres= new HashSet();
   
}
