/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
<<<<<<< HEAD
=======
import javax.persistence.NamedQueries;
>>>>>>> b08685cb28e3c58946a224d948c140ec9244f467
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author FADOUA
 */
@Entity
@XmlRootElement
<<<<<<< HEAD
@NamedQuery(name="Type.findAll", query="SELECT t FROM Type t")
=======
@NamedQueries({
@NamedQuery(name="Type.find", query="SELECT e FROM Type e where e.type= 'document'") })
@NamedQuery(name="Type", query="select t from Type t where t.type= :param")
>>>>>>> b08685cb28e3c58946a224d948c140ec9244f467
public class Type implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Publication> getPubliation() {
        return Publiation;
    }

    public void setPubliation(Set<Publication> Publiation) {
        this.Publiation = Publiation;
    }
    
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idType")
    private Set<Publication> Publiation= new HashSet();
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Type)) {
            return false;
        }
        Type other = (Type) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entites.Type[ id=" + id + " ]";
    }
    
}
