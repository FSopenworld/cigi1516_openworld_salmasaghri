/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
<<<<<<< HEAD
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

=======
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;
>>>>>>> b08685cb28e3c58946a224d948c140ec9244f467

@Entity
<<<<<<< HEAD
@NamedQueries({
@NamedQuery(name="OffreEntreprise.findAll", query="SELECT e FROM Offre e where e.idEntreprise = :param"),
@NamedQuery(name="Actualite.find", query="SELECT e FROM Offre e where e.idEntreprise = :param and e.dateExperation > :date"),})

@NamedNativeQueries({
@NamedNativeQuery(name="NbPostulantOffre", query="select count(Etudiants_ID) from etudiant_offre where Offres_ID= ?1")
})

=======
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "offre.findByoffre", query = "SELECT u FROM Offre u WHERE u.typeOffre = :param"),
 @NamedQuery(name="Offre.findAll", query="select o from Offre o"),
       @NamedQuery(name="Offre.stage", query="select o from Offre o where o.typeOffre = :param"),
        @NamedQuery(name="Offre.emploi", query="select o from Offre o where o.typeOffre = :param")})
>>>>>>> b08685cb28e3c58946a224d948c140ec9244f467
public class Offre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String offre;
    private String secteur;
    private String datePublication;
    private String dateExperation;
    private String typeOffre;
    
    @ManyToOne(optional = false)
    private Entreprise idEntreprise;
    
    @ManyToOne(optional = false)
    private Enseignant idEnseignant;
    
    @ManyToOne(optional = false)
    private Etudiant idLaureat;

    public Etudiant getIdLaureat() {
        return idLaureat;
    }

    public void setIdLaureat(Etudiant idLaureat) {
        this.idLaureat = idLaureat;
    }
    
    @ManyToMany(mappedBy = "Offres")
    private Set<Etudiant> Etudiants=new HashSet();

    public Set<Etudiant> getEtudiants() {
        return Etudiants;
    }

    //La liste des etudiants qui ont postulé à un offre
    public void setEtudiants(Set<Etudiant> Etudiants) {
        this.Etudiants = Etudiants;
    }
    
    

    public String getOffre() {
        return offre;
    }

    public void setOffre(String offre) {
        this.offre = offre;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getDateExperation() {
        return dateExperation;
    }

    public void setDateExperation(String dateExperation) {
        this.dateExperation = dateExperation;
    }

    public String getTypeOffre() {
        return typeOffre;
    }

    public void setTypeOffre(String typeOffre) {
        this.typeOffre = typeOffre;
    }

  

    public Entreprise getIdEntreprise() {
        return idEntreprise;
    }

    public void setIdEntreprise(Entreprise idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public Enseignant getIdEnseignant() {
        return idEnseignant;
    }

    public void setIdEnseignant(Enseignant idEnseignant) {
        this.idEnseignant = idEnseignant;
    }

  
    
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offre)) {
            return false;
        }
        Offre other = (Offre) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entites.Offre[ id=" + id + " ]";
    }
    
}
