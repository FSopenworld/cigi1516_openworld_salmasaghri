/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author ibn brahim
 */
@Entity  
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
@NamedQuery(name="Etudiant.findAll", query="SELECT e FROM Etudiant e") ,
@NamedQuery(name="Etudiant.findByCNE", query="select e from Etudiant e where e.CNE = :cne"),
@NamedQuery(name = "Etudiant.findByLogin", query = "SELECT u FROM Etudiant u WHERE u.login = :login AND u.password= :mdp")})

public class Etudiant extends Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @Column(unique=true)
    private String CNE;
    private String prenom;
    private String cv;
   
    private int active;
    
    //Parcours
    @ManyToOne(optional = false)
    private Parcours idParcours;
    
    //liste diplomes
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEtudiant")
    private Set<Diplome> Diplomes= new HashSet();
    
    //liste Experiences
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEtudiant")
    private Set<Experience> Experiences= new HashSet();
    
    //ListeActivtes
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEtudiant")
    private Set<Activites> Activites= new HashSet();
    
    @ManyToMany
    private Set<Offre> Offres=new HashSet();

    public Set<Offre> getOffres() {
        return Offres;
    }

    public void setOffres(Set<Offre> Offres) {
        this.Offres = Offres;
    }
    
    
    public Parcours getIdParcours() {
        return idParcours;
    }

    public void setIdParcours(Parcours idParcours) {
        this.idParcours = idParcours;
    }

    public Set<Diplome> getDiplomes() {
        return Diplomes;
    }

    public void setDiplomes(Set<Diplome> Diplomes) {
        this.Diplomes = Diplomes;
    }

    public Set<Experience> getExperiences() {
        return Experiences;
    }

    public void setExperiences(Set<Experience> Experiences) {
        this.Experiences = Experiences;
    }

    public Set<Activites> getActivites() {
        return Activites;
    }

    public void setActivites(Set<Activites> Activites) {
        this.Activites = Activites;
    }
    
    
    public String getCNE() {
        return CNE;
    }

    public void setCNE(String CNE) {
        this.CNE = CNE;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
    
 

    

    
}
