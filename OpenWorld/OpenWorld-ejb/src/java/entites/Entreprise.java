
package entites;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@NamedQueries({
@NamedQuery(name="Entreprise.findAll", query="SELECT e FROM Entreprise e where e.verifie=1")  ,
@NamedQuery(name="Entreprise.findAllEntrNonVerif", query="SELECT et FROM Entreprise et where et.verifie=0") ,
@NamedQuery(name = "Entreprise.findByLogin", query = "SELECT u FROM Entreprise u WHERE u.login = :login AND u.password= :mdp")})

@NamedNativeQueries({
@NamedNativeQuery(name="getNbEntrNonVerif", query="select count(*) from entreprise where verifie=0")
})
public class Entreprise extends Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(unique=true)
    private String codeRef;
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idEntreprise")
    private Set<Offre> Offres= new HashSet();
    
    private boolean verifie;
    private String activite;

    public String getActivite() {
        return activite;
    }

    public void setActivite(String activite) {
        this.activite = activite;
    }
    
    public boolean isVerifie() {
        return verifie;
    }

    public void setVerifie(boolean verifie) {
        this.verifie = verifie;
    }

    public String getCodeRef() {
        return codeRef;
    }

    public void setCodeRef(String codeRef) {
        this.codeRef = codeRef;
    }

    public Set<Offre> getOffres() {
        return Offres;
    }

    public void setOffres(Set<Offre> Offres) {
        this.Offres = Offres;
    }
    
}
