/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EspaceEnseignant;

import entites.Departement;
import entites.Type;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ibn brahim
 */
@Stateless
public class SLEspaceEnseignant implements SLEspaceEnseignantLocal {

   @PersistenceContext(unitName = "OpenWorld-ejbPU")
    private EntityManager em;
   
   @Override
    public List<Type> ListeType() {
        //selectionner tous les dpartement de l'itablissement
        Query query = em.createNamedQuery("Type.findAll",Type.class);
          return query.getResultList();
    }
   
}
