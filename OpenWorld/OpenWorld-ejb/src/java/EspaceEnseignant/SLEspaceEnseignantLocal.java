/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EspaceEnseignant;

import entites.Type;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ibn brahim
 */
@Local
public interface SLEspaceEnseignantLocal {
     public List<Type> ListeType();
}
