/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionEtudiant;

import entites.Activites;
import entites.Departement;
import entites.Diplome;
import entites.Etudiant;
import entites.Experience;
import entites.Laureat;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pc
 */
@Stateless
public class Gestionetudiant implements GestionetudiantLocal {
@PersistenceContext( unitName = "OpenWorld-ejbPU" )
    private EntityManager   em;
    @Override
    public Type TypePub() {
        Query req=em.createNamedQuery("Type.find",Type.class);
       List<Type> type=req.getResultList();
       return type.get(0);
    }

    @Override
    public void ajoutpublication(Publication p) {
        em.persist(p);
    }
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public List<Publication> listdocuments() {
       
        
         Query req=em.createNamedQuery("Publication.find",Publication.class);
       List<Publication> pub=req.getResultList();
       return pub;
    }

    @Override
    public List<Offre> listoffre() {
Query req=em.createNamedQuery("offre.findByoffre",Offre.class);
       req.setParameter("param",0);
       List<Offre> pub=req.getResultList();
       return pub;    
    }
    @Override
    public Offre updateoffre(Offre O ){
         return em.merge(O);
    }

    @Override
    public Etudiant updateE(Etudiant e) {
        return em.merge(e);
    }

    @Override
    public void ajouterExperience(Experience e) {
        em.persist(e);
    }

    @Override
    public void ajouterDiplome(Diplome e) {
  em.persist(e);    }

    @Override
    public void ajouterActivité(Activites e) {
  em.persist(e);    }
}
