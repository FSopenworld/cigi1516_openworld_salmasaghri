/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionEtudiant;

import entites.Activites;
import entites.Diplome;
import entites.Etudiant;
import entites.Experience;
import entites.Offre;
import entites.Publication;
import entites.Type;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author pc
 */
@Local
public interface GestionetudiantLocal {
   public Type  TypePub();
   public void ajoutpublication(Publication p);
   public List<Publication> listdocuments();
   public List<Offre> listoffre();
    public Offre updateoffre(Offre O );
    public Etudiant updateE(Etudiant e);
    public void ajouterExperience(Experience e);
    public void ajouterDiplome(Diplome e);
    public void ajouterActivité(Activites e);
}
